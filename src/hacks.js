console.log('Start reddit-extras');

const exist = document.getElementById('reddit-extras');
if (exist) exist.remove();

const downAll = document.createElement('button');
downAll.className = 'button';
downAll.textContent = 'down all';
downAll.onclick = () => {
  Array.from(document.getElementsByClassName('down arrow')).forEach((a) => a.click());
};

const resetAll = document.createElement('button');
resetAll.className = 'button';
resetAll.textContent = 'reset all';
resetAll.onclick = () => {
  Array.from(document.getElementsByClassName('arrow downmod')).forEach((a) => a.click());
  Array.from(document.getElementsByClassName('arrow upmod')).forEach((a) => a.click());
};

const upAll = document.createElement('button');
upAll.className = 'button';
upAll.textContent = 'up all';
upAll.onclick = () => {
  Array.from(document.getElementsByClassName('up arrow')).forEach((a) => a.click());
};
const blockAll = document.createElement('button');
blockAll.className = 'button';
blockAll.textContent = 'block all';
blockAll.onclick = () => {
  const timer = setInterval(() => {
    const a = Array.from(document.getElementsByClassName('block-user-link'))[0];
    if (a) {
      a.click();
    } else {
      clearInterval(timer);
    }
  }, 350);
};

const extras = document.createElement('div');
extras.id = 'reddit-extras';
extras.className = 'reddit-extras';
extras.appendChild(downAll);
extras.appendChild(resetAll);
extras.appendChild(upAll);
extras.appendChild(blockAll);

document.body.appendChild(extras);

Array.from(document.getElementsByClassName('flat-list buttons')).forEach((bl) => {
  const a = document.createElement('li');
  a.innerHTML = `
  <form class="block-button " action="#" method="get">
  <input name="executed" type="hidden" value="blocked">
  <a class="block-user-link" href="javascript:void(0)" onclick='change_state(this, "block", hide_thing, undefined, null)'>block user</a>
  </form>
  `;
  bl.prepend(a);
});

Array.from(document.getElementsByClassName('expando-button collapsed')).forEach((a) => a.click());

setTimeout(() => {
  Array.from(document.getElementsByTagName('video')).forEach((a) => a.pause());
}, 500);

console.log('Finished');
